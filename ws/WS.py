
import re #Para o split com \W+
import argparse
import sys
import socket
import os
import signal

BUFFER_SIZE = 1024

#criar DIRECTORIES
try:
	output_dir = 'output_files'
	if not os.path.exists(output_dir):
		os.makedirs(output_dir)
except OSError as rr:
	print("Error : creating output_files directory: ", err)

try:
	input_dir = 'input_files'
	if not os.path.exists(input_dir):
		os.makedirs(input_dir)
except OSError as err:
	print("ERROR : creating input_files directory: ", err)


def get_args():
	try:
		parser = argparse.ArgumentParser()
		parser.add_argument("PTC",choices=["WCT","FLW","UPP","LOW"],nargs='*',
		help="WCT - Word count. FLW - Find longest word.\
		UPP- Convert to upper case. LOW - Convert to lower case")
		parser.add_argument("-p","--WSport",type=int, help="Working server port.")
		parser.add_argument("-n","--CSname",type=str, help="Central server name.")
		parser.add_argument("-e","--CSport",type=int, help="Central server port.")
		args = parser.parse_args()
		return(args.PTC, args.WSport, args.CSname, args.CSport)
	except TypeError as err:
		print("get_args: ", err)
		exit()

#WCT
def word_count(filename):
	fl = open(filename,"r")
	text = fl.read()
	fl.close()
	words = re.split('\W+',text) #\W+ remove pontos e virgulas e espacos e \n
	#Se o ultimo character for um . a ultima posicao e uma palavra vazia
	numwords = len(words)
	print("\tNumber of words: ",numwords)
	return numwords



#FLW
def find_longest_word(filename):
	fl = open(filename,"r")
	text = fl.read()
	words = re.split('\W+',text) #\W+ remove pontos e virgulas e espacos e \n
	longest =''
	size = 0
	for w in words:
		wsize = len(w)
		if wsize > size:
			longest = w
			size = wsize
	print("\tLongest:", longest)
	fl.close()
	return longest


#UPP LOW
def convert_text(filename,to_type):
	fl = open(filename,"r+")
	if to_type == "UPPER":
		text = fl.read()
		converted_text = text.upper()
	elif to_type == "LOWER":
		text = fl.read()
		converted_text = text.lower()
	ct_msg = "REP F " + repr(fl.tell()) + " " + fl.read()
	print(ct_msg)
	fl.close()
	return converted_text


def read_tcp(sock,address):
	fullmsg = ""
	dataArray = []
	try:
		while 1:
			data = sock.recv(BUFFER_SIZE)
			if not data: break
			sock.settimeout(1)
			string = data.decode('utf-8')
			fullmsg += string
			dataArray.append(string)
	except OSError as err:
		print('Connection closed with central server.')
	inputArray = dataArray[0].split()
	dataStart = (len(inputArray[0]) + 1 +
	len(inputArray[1]) + 1 +
	len(inputArray[2]) + 1 +
	len(inputArray[3]) + 1) #calcular o inicio do ficheiro na string
	if inputArray[0] == "WRQ":
		filename = "./input_files/" + inputArray[2] #Criar o ficheiro
		fl = open(filename,"w")
		fl.write(fullmsg[dataStart:])
		fl.close()
	else:
		try:
			sock.sendall("WRP ERR\n")
			sock.close()
			return
		except IOError as err:
			print('read_tcp: WRP ERR: ', err)
	op = inputArray[1]
	tcp_msg = "REP "
	processed_text = ''
	if op == "WCT" and WCT:
		tcp_msg = tcp_msg + "R"
		processed_text = word_count(filename)
	elif op == "FLW" and FLW:
		tcp_msg = tcp_msg + "R"
		processed_text = find_longest_word(filename)
	elif op == "UPP" and UPP:
		tcp_msg = tcp_msg + "F"
		processed_text = convert_text(filename, "UPPER")
	elif op == "LOW" and LOW:
		tcp_msg = tcp_msg + "F"
		processed_text = convert_text(filename, "LOWER")
	else:
		try:
			sock.sendall("WRP EOF\n")
			sock.close()
			return
		except IOError as err:
			print('read_tcp: WRP EOF: ', err)
	try:
		size = len(str(processed_text).encode('utf-8'))
		tcp_msg += " " + str(size) + " "
		sock.sendall(tcp_msg.encode('utf-8')) # send first part of message
		tcp_msg = str(processed_text)
		sock.sendall(tcp_msg.encode('utf-8')) # send file message
		sock.close()
		return
	except IOError as err:
		print('read_tcp: sending answer: ', err)
		return


def sigHandler(a,b):
	sig_msg = "UNR " + wsaddress[0] + " " + repr(wsaddress[1]) + "\n"
	msg="UAK NOK\n".encode('utf-8')
	try:
		udp_sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
		udp_sock.sendto(sig_msg.encode('utf-8') , csaddress)
		udp_sock.settimeout(5) #Se o CS tenha sido parado antes de o WS
		msg,addr = udp_sock.recvfrom(BUFFER_SIZE)
	except OSError as err:
		print("sigHandler: sending msg: ",err)
	except socket.error as err:
		print("sigHandler: sending msg: ",err)
	print(msg.decode('utf-8'),end="")
	try:
		udp_sock.close()
		tcp_sock.close()
		exit()
	except OSError as err:
		print("sigHandler: exiting: ", err)
	exit()


#MAIN
sock=socket.socket()
filename = ("./TestFiles/test.txt")
WCT = False
FLW= False
UPP= False
LOW = False
PTC,wsport,csname,csport = get_args()
print(PTC,wsport,csname,csport,)


#Default Values
if wsport == None:
    wsport = 59000
if csname == None:
    csname = 'localhost'
if csport == None:
    csport = 58061
udp_msg = "REG"

try:
    csip = socket.gethostbyname(csname)
    wsname = socket.gethostname()
    wsname = socket.gethostbyname(wsname)
    print(PTC,wsname,wsport,csname,csport,csip)
except OSError as err:
    print ("gethost: ", err)
    exit()

for s in PTC: 	#"and not PTC" é para na mensagem não aparecer repetido caso
				# iniciem os WS com PTCs repetidas
    if s == 'WCT' and not WCT:
        WCT = True
        udp_msg = udp_msg +" WCT"
    elif s == 'UPP' and not UPP:
        UPP = True
        udp_msg = udp_msg +" UPP"
    elif s == 'FLW' and not FLW:
        FLW = True
        udp_msg = udp_msg +" FLW"
    elif s == 'LOW' and not LOW:
        LOW = True
        udp_msg = udp_msg +" LOW"
print("WCT on: ",WCT,"\nFLW on: ",FLW,"\nUPP on: ",UPP,"\nLOW on: ",LOW)

#Main register WS

csaddress =(csip,csport)
def register(reg_msg):
	try:
	    udp_sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
	except OSError as err:
	    print("Creating udp_sock: ",err)
	    exit()

	signal.signal(signal.SIGINT,sigHandler)
	signal.signal(signal.SIGTERM,sigHandler)
	reg_msg = reg_msg + " " + wsname + " " + repr(wsport) +"\n"
	print(reg_msg,end="")
	try:
	    udp_sock.sendto(reg_msg.encode('utf-8') , csaddress)
	    reg_msg,addr = udp_sock.recvfrom(BUFFER_SIZE)
	    reg_msg = reg_msg.decode('utf-8')
	    print(reg_msg,end="")
	    for string in reg_msg.split():
	        if string == "NOK":
	            print("Failed to register WS")
	            exit()
	except OSError as err:
	    print("Sending/Receiving udp_sock: ",err)
	    exit()

#Main TCP connection
wsaddress = (wsname,wsport)
try:
    tcp_sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    tcp_sock.bind(wsaddress)
    tcp_sock.listen(1)
except OSError as err:
    print("Creating tcp_sock: ",err)
    exit()

register(udp_msg)

while 1:
    try:
        new_tcp_socket, new_socket_address = tcp_sock.accept()
        wpid = os.fork()
    except OSError as err:
        print(err)
    if wpid == 0: #Child process
        try:
            tcp_sock.close()
        except OSError as err:
            print("tcp close in child", err)
        read_tcp(new_tcp_socket, new_socket_address)
        exit()
    else: #Parent process
        try:
            new_tcp_socket.close()
        except OSError as err:
            print("new tcp close in parent: ", err)
