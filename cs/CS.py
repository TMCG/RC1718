import socket
import sys
import argparse
import select
import os
import re
import time

#CONSTANTS
BUFFER_SIZE = 1024

#AUXILIARY FILES AND DIRECTORIES
try:
	fpt = open('fileprocessingtasks.txt', 'w').close() # Limpar o ficheiro
	req_number = open('req_number.txt', 'w')
	req_number.write('1')
	req_number.close()
except OSError as err:
	print("ERROR : fileprocessingtasks, req_number: ", err)
try:
	input_dir = 'input_files'
	if not os.path.exists(input_dir):
		os.makedirs(input_dir)
except OSError as err:
	print("ERROR : creating input_files directory: ", err)
try:
	output_dir = 'output_files'
	if not os.path.exists(output_dir):
		os.makedirs(output_dir)
except OSError as err:
	print("Error : creating output_files directory: ", err)


def get_args():
	try:
		parser = argparse.ArgumentParser()
		parser.add_argument("-p", "--port", type = int, help="CSport")
		args = parser.parse_args()
		if not (args.port):
			return 58061
		return (args.port)
	except TypeError as err:
		print("get_args: ", err)
		exit()

port = get_args()

def read_tcp(sock,addr):
	#print("DEBUG: TCP_SOCKET accept")
	recv_string = ''
	while 1:
		try:
			data = sock.recv(BUFFER_SIZE)
			recv_string += data.decode('utf-8')
			sock.settimeout(0.1)
		except socket.timeout:
			if recv_string == '':
				print("No data received, closing socket...")
				return
			else:
				break
		except UnicodeDecodeError as err:
			break

	#print("DEBUG(recv_string): ", recv_string)
	command = recv_string.split()
	if command[0] == "LST":
		if len(command) == 1:
			print("List request: " + str(addr[0]) + " " + str(addr[1]))
			ptc_list = get_available_ptc()
			sock.sendall(ptc_list.encode('utf-8'))
		else:
			sock.sendall("FPT ERR\n".encode('utf-8'))
	elif command[0] == "REQ":
		request = command[1:]
		#print("DEBUG: len request = " + str(len(request)))
		handle_request(request, sock, recv_string,addr)
	else:
		print("Command not recognized")

def read_udp(sock):
	data, addr = sock.recvfrom(BUFFER_SIZE)
	command = data.decode('utf-8').rstrip().split()
	if command[0] == "REG":
		ws_info = ""
		for ptc in command:
			if ptc == "WCT":
				ws_info += "WCT " + command[-2] + " " + command[-1] + "\n"
				continue
			elif ptc == "UPP":
				ws_info += "UPP " + command[-2] + " " + command[-1] + "\n"
				continue
			elif ptc == "LOW":
				ws_info += "LOW " + command[-2] + " " + command[-1] + "\n"
				continue
			elif ptc == "FLW":
				ws_info += "FLW " + command[-2] + " " + command[-1] + "\n"
				continue
		fpt = open('fileprocessingtasks.txt', 'a')
		ack_ws_reg = ""
		for string in command[1:]:
			ack_ws_reg += string + " "
		print('+' + ack_ws_reg)
		fpt.write(ws_info)
		fpt.close()
		sock.sendto("RAK OK\n".encode('utf-8'), addr)
	elif command[0] == "UNR":
		fpt = open('fileprocessingtasks.txt', 'r')
		lines = fpt.readlines()
		fpt.close()
		fpt = open('fileprocessingtasks.txt', 'w')
		unr_sv = command[-2] + " " + command[-1]
		rm_info = '-'
		for line in lines:
			if unr_sv not in line:
				fpt.write(line)
			else:
				rm_info += line[0:3] + " "
		rm_info += unr_sv
		print(rm_info)
		fpt.close()
		sock.sendto("UAK OK \n".encode('utf-8'), addr)

def get_available_ptc():
	fpt = open('fileprocessingtasks.txt', 'r')
	n_ptc = 0
	reply = "FPT"
	ptc = ""
	WCT = False
	FLW= False
	UPP= False
	LOW = False
	for line in fpt:
		line = re.split('\W+', line)
		if line[0] == "WCT":
			WCT = True
			continue
		elif line[0] == "UPP":
			UPP = True
			continue
		elif line[0] == "LOW":
			LOW = True
			continue
		elif line[0] == "FLW":
			FLW = True
			continue
	fpt.close()
	if WCT:
		n_ptc += 1
		ptc += " WCT"
	if FLW:
		n_ptc += 1
		ptc += " FLW"
	if UPP:
		n_ptc += 1
		ptc += " UPP"
	if LOW:
		n_ptc += 1
		ptc += " LOW"
	if n_ptc == 0:
		ptc += " EOF"
		return reply + ptc + "\n"
	return reply + " " + str(n_ptc) + ptc + "\n"

def handle_request(request, sock, recv_string,address):
	try:
		if is_available_ptc(request[0]):
			recv_string_array = recv_string.split()
			text_to_remove = (len(recv_string_array[0]) + 1 +
							  len(recv_string_array[1]) + 1 +
							  len(recv_string_array[2]) + 1)
			recv_string = recv_string[text_to_remove:]
			req = open('req_number.txt', 'r')
			req_id = int(req.readline())
			req.close()
			req = open('req_number.txt', 'w')
			req.write(str(req_id + 1))
			req.close()
			filename = "./input_files/%05i.txt" % req_id
			input_file = open(filename, 'w')
			input_file.write(recv_string)
			input_file.close()
			#msg = "WRQ " + request[0] + " " + "%05i.txt " % req_id + request[1] + " "
			#Ligar ao working server
			wsList = get_available_ws(request[0])
			#print("DEBUG: ", wsList)
			num_ws = len(wsList)
			#Dividir o ficheiro em fragmentos
			num_lines = 0
			input_file = open(filename, 'r')
			for line in input_file:
				num_lines += 1
			lines_per_frag = num_lines // (num_ws)
			frag_ws_list = []
			for ws in wsList:
				ws_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
				ws_sock.connect((ws[0], int(ws[1])))
				frag_ws_list.append(ws_sock)
			#print("DEBUG: ", frag_ws_list)
			fl = open(filename,"r")
			for i in range(num_ws): #todos menos o ultimo
				frag_data = ''
				frag_filename = "%05i%03i.txt" % (req_id, i+1)
				if i < (num_ws - 1):
					for l in range(lines_per_frag):
						frag_data += fl.readline()
					size = len(frag_data.encode('utf-8'))
					msg = "WRQ " + request[0] + " " + frag_filename + " " + str(size) + " "
					frag_ws_list[i].sendall(msg.encode('utf-8'))
					#print("DEBUG(frag_data): ", frag_data)
					frag_ws_list[i].sendall(frag_data.encode('utf-8'))
				else:
					frag_data = fl.read()
					size = len(frag_data.encode('utf-8'))
					msg = "WRQ " + request[0] + " " + frag_filename + " " + str(size) + " "
					frag_ws_list[i].sendall(msg.encode('utf-8'))
					#print("DEBUG(frag_data): ", frag_data)
					frag_ws_list[i].sendall(frag_data.encode('utf-8'))
			num_fragment = 0
			while num_ws > 0:
				readable, writable, exceptional = select.select(frag_ws_list, [], frag_ws_list)
				for frag_sock in readable:
					#print("DEBUG(sock): ", frag_sock.getpeername())
					if frag_sock is frag_ws_list[0]:
						reply = ''
						while 1:# receive file
							data = frag_sock.recv(BUFFER_SIZE)
							if not data: break #data"""
							reply += data.decode('utf-8')
						filename = "./output_files/%05i%03i.txt" % (req_id, num_fragment+1)
						open(filename, 'w').close() # clear file
						with open(filename, 'w') as frag:
							#print("DEBUG: ",reply)
							replyArray = reply.split()
							reply_wo_file = replyArray[0] + " " + replyArray[1] + " " + replyArray[2] + " "
							len_to_remove = len(replyArray[0]) + 1 + len(replyArray[1]) + 1 + len(replyArray[2]) + 1
							reply = reply[len_to_remove:]
							frag.write(reply)
							frag_ws_list.remove(frag_sock)
						num_fragment += 1
						frag_sock.close()
						num_ws -= 1
					#print("DEBUG: " , num_ws)
			if request[0] == 'WCT':
				print("WCT request: " + str(address[0]) + " " + str(address[1]))
				total_words = 0
				for i in range(num_fragment):
					frag_filename = "./output_files/%05i%03i.txt" % (req_id, i+1)
					with open(frag_filename, 'r') as f:
						total_words += int(f.read())
				size = len(str(total_words).encode('utf-8'))
				msg = 'REP R ' + str(size) + ' '
				sock.sendall(msg.encode('utf-8'))
				#time.sleep(1)
				reply = str(total_words) + '\n'
				sock.sendall(reply.encode('utf-8'))
			elif request[0] == 'FLW':
				print("FLW request: " + str(address[0]) + " " + str(address[1]))
				longest_word = ''
				for i in range(num_fragment):
					frag_filename = "./output_files/%05i%03i.txt" % (req_id, i+1)
					with open(frag_filename, 'r') as f:
						word = f.read()
						if len(word) > len(longest_word):
							longest_word = word
				size = len(longest_word.encode('utf-8'))
				msg = 'REP R ' + str(size) + ' '
				#print("DEBUG: ", msg)
				sock.sendall(msg.encode('utf-8'))
				time.sleep(1)
				sock.sendall(longest_word.encode('utf-8'))
			elif request[0] == 'UPP':
				print("UPP request: " + str(address[0]) + " " + str(address[1]))
				total_size = 0
				filename = "./output_files/%05i.txt" % req_id
				open(filename, 'w').close() # Limpar ficheiro se existir
				for i in range(num_fragment):
					frag_filename = "./output_files/%05i%03i.txt" % (req_id, i+1)
					with open(frag_filename, 'r') as frag_f:
						with open(filename, 'a+') as f:
							f.write(frag_f.read())
				msg = 'REP F ' + repr(os.stat(filename).st_size) + ' '
				sock.sendall(msg.encode('utf-8'))
				time.sleep(1)
				with open(filename, 'r') as f:
					sock.sendall(f.read().encode('utf-8'))
			elif request[0] == 'LOW':
				print("LOW request: " + str(address[0]) + " " + str(address[1]))
				total_size = 0
				filename = "./output_files/%05i.txt" % req_id
				open(filename, 'w').close() # Limpar ficheiro se existir
				for i in range(num_fragment):
					frag_filename = "./output_files/%05i%03i.txt" % (req_id, i+1)
					with open(frag_filename, 'r') as frag_f:
						with open(filename, 'a+') as f:
							f.write(frag_f.read())
				msg = 'REP F ' + repr(os.stat(filename).st_size) + ' '
				sock.sendall(msg.encode('utf-8'))
				time.sleep(1)
				with open(filename, 'r') as f:
					sock.sendall(f.read().encode('utf-8'))
			else:
				sock.sendall("REP ERR\n".encode('utf-8'))
		else:
			sock.sendall("REP EOF\n".encode('utf-8'))
	except OSError as err:
		print("handle_request: ", err)


def get_available_ws(ptc):
	fpt = open('fileprocessingtasks.txt', 'r')
	wsArray = []
	for line in fpt:
		line = line.split()
		if line[0] == ptc:
			wsArray.append((socket.gethostbyname(line[1]), int(line[2])))
	fpt.close()
	return wsArray

def is_available_ptc(ptc):
	fpt = open('fileprocessingtasks.txt', 'r')
	for line in fpt:
		line = re.split('\W+', line)
		if line[0] == ptc:
			fpt.close()
			return True
	fpt.close()
	return False


#print("DEBUG: Waiting for connection")
#print("DEBUG: " + str(port))
try:
	tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	tcp_socket.bind(('',port));
	tcp_socket.listen(1)
	udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	udp_socket.bind(('', port));
	inputs = [tcp_socket, udp_socket]
except OSError as err:
	print("creating tcp and udp", err)
	exit()

while 1:
	readable, writable, exceptional = select.select(inputs, [], inputs)
	for sock in readable:
		if sock is tcp_socket:
			try:
				new_tcp_socket, new_tcp_address = tcp_socket.accept()
				wpid = os.fork()
			except OSError as err:
				print(err)
			if wpid == 0:
				try:
					tcp_socket.close()
				except OSError as err:
					print("tcp close in child", err)
				read_tcp(new_tcp_socket, new_tcp_address)
				exit()
			else:
				try:
					new_tcp_socket.close()
				except OSError as err:
					print("new tcp close in parent: ", err)
		elif sock is udp_socket:
			read_udp(sock)
		else:
			print("dont talk to me or my socket ever again")
