import socket
import sys
import argparse
import string
import os

BUFFER_SIZE = 1024
CSport = 58061

def get_args():
	try:
		parser = argparse.ArgumentParser()
		parser.add_argument("-p", "--port", type = int, help="socket port")
		parser.add_argument("-n" , "--csname", help="Central Server name")
		args = parser.parse_args()
		if not args.port and not args.csname:
			return (CSport, "localhost")
		elif not args.csname:
			return(args.port, "localhost")
		elif not args.port:
			return(CSport, args.csname)
		else:
			return (args.port, args.csname)
	except TypeError as err:
		print("get_args: ", err)
		exit()

def generate_message(array):
	try:
		j = 0
		newMessage = []
		for str in array:
			i = j + 1
			num = repr(i)
			if str == 'WCT':
				newMessage.append(num + '- WCT - word count')
			elif str == 'UPP':
				newMessage.append(num + '- UPP - convert to upper case')
			elif str == 'FLW':
				newMessage.append(num + '- FLW - find longest word')
			elif str == 'LOW':
				newMessage.append(num + '- LOW - convert to lower case')
			j+= 1
		return newMessage
	except OSError as err:
		print('generate_message: ', err)
		exit()


def gethost(args):
	try:
		address = args[1]
		ip = socket.gethostbyname(address) # GrabIP devemos ir buscar a linha de argumento
		return ip
	except OSError as err:
		print('gethost: ', err)
		exit()

def list_commands():
	try:
		sock.sendall(b'LST\n') #criar funcao para send e receive
		while 1:
			data = sock.recv(BUFFER_SIZE)
			if not data: break #data is a bytes object
			array = data.decode('utf-8').split()
			if array[1] == 'EOF':
				raise ValueError('Request could not be anwsered.')
			elif array[1] == 'ERR':
				raise ValueError('Request not correctly formulated.')
			else:
				fixedMessage = array[2:]
				formatedMessage = generate_message(fixedMessage)
				for str in formatedMessage:
					print(str)
		sock.close()
		return
	except OSError as msg:
		print('list_commands: ', msg)
	except ValueError as err:
		print('list_commands: ', err)

def request(command):
	try:
		size = os.stat(command[2]).st_size
		if command[1] == "UPP" or command[1] == "LOW":
			print(str(size) + ' Bytes to transmit')
		comm  = 'REQ ' + command[1] + ' ' + str(size) + ' '
		commandLine = bytes(comm, 'utf-8')
		with open(command[2], 'rb') as file_to_send:
			sock.sendall(commandLine)
			for data in file_to_send:
				sock.sendall(data)
		string = ''
		while 1:
			data = sock.recv(BUFFER_SIZE)
			if not data: break #data
			string += data.decode('utf-8')
		dataArray = string.split()
		outputFormat = dataArray[0:3]
		dataArray = dataArray[3:]
		length = len(outputFormat)
		if length > 2:
			len_to_remove = len(outputFormat[0]) + 1 + len(outputFormat[1]) + 1 + len(outputFormat[2]) + 1
			string = string[len_to_remove:]
			length = len(outputFormat)
			if outputFormat[1] == "R":
				if command[1] == 'WCT':
					print('Number of words:', string)
				elif command[1] == 'FLW':
					print('Longest word:', string)
			elif outputFormat[1] == 'F':
				name = command[2].replace('.txt', '')
				if command[1] == 'UPP':
					file_name = name + '_UPP.txt'
				elif command[1] == 'LOW':
					file_name = name + '_LOW.txt'
				text_file = open(file_name, 'w')
				text_file.write(string)
				print('received file ' + file_name)
				print(outputFormat[2] + ' Bytes')
		elif length == 1 and outputFormat[0] == 'ERR':
			print("ERR")
		elif length == 2:
			if outputFormat[1] == "ERR":
				print("Request was not correctly formulated.")
			elif outputFormat[1] == "EOF":
				print("Request could not be awnsered.")
		sock.close()
		return
	except IndexError as err:
		print('request: BAD AWNSER FROM CENTRAL SERVER ', err)
		#sock.close()
		#return
	except IOError as err:
		print('request: ', err)
		sock.close()
		return
	except OSError as err:
		print('request: ', err)
		sock.close()
		exit()

args = get_args()
ip = gethost(args)
port = args[0]
while 1:
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	command = None
	while not command:
		input_line = input()
		command = input_line.split()
		sock.connect((ip, port))
	if command[0] == 'list':
		#pedir ao servidor quais aos servicos
		list_commands()
	elif command[0] == 'request':
		request(command)
		#close sockets
	elif command[0] == 'exit':
		sock.close()
		exit()
	else:
		sock.close()
		print('Input Error: Command not supported')
